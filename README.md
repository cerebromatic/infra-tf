<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

<!-- PROJECT LOGO -->
<br />
<p align="center">
  <h3 align="center">Cerebromatic Infrastructure Terraform</h3>
</p>


<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
Cerebromatic, as it currently exists, is a set of scripts and terraform modules
for setting up and managing a kubernetes platform running on Google Cloud Platform.


### Built With
* [Terraform](https://www.terraform.io/)
* [Google Cloud Platform](https://cloud.google.com/)
* [Gitlab Ci/CD](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/)

## Coming Soon
* [Kubernetes](https://kubernetes.io/)
* [Helm](https://helm.sh)

<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

* A Google Cloud account
* Your Google Cloud Organization Id
* Your billing account id
* gcloud cli installed
* terraform installed

### Installation

1. Set some important environment variables
```sh
export GCP_ORG_ID="your google org id"
export GCP_BILLING_ACCOUNT="your google billing account id"
export ADMIN_PROJECT="project-name"
```
 2. Create the Terraform Admin project
 ```sh
./create_tf_admin_project.sh
```

<!-- USAGE EXAMPLES -->
## Usage

To create projects that are each managed by the admin project...

1. Add the following environment variables to a local .env file. (Do NOT commit this!)
```sh
cat << EOF >> .env
export GCP_ORG_ID="your google org id"
export GCP_BILLING_ACCOUNT="your google billing account id"
export TF_VAR_org_id=${GCP_ORG_ID}
export TF_VAR_billing_account=${GCP_BILLING_ACCOUNT}
export TF_ADMIN=${ADMIN_PROJECT}-terraform-admin
export TF_CREDS=~/.config/gcloud/${TF_ADMIN}.json
export GOOGLE_APPLICATION_CREDENTIALS=${TF_CREDS}
export GOOGLE_PROJECT=${TF_ADMIN}
EOF
source .env
```
2. Initialize the terraform project
```sh
terraform init
```
3. Create a plan to see what changes will be applied
```sh
terraform plan
```
4. If everything looks good, apply the plan
```sh
terraform apply
```

<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/cerebromatic/infra-tf/-/issues) for a list of proposed features (and known issues).



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Piper Townley - piper@cerebromatic.com

[![LinkedIn][linkedin-shield]][linkedin-url]

Project Link: [https://gitlab.com/cerebromatic/infra-tf](https://gitlab.com/cerebromatic/infra-tf)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [GitHub Emoji Cheat Sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet)
* [Img Shields](https://shields.io)
* [Choose an Open Source License](https://choosealicense.com)
* [GitHub Pages](https://pages.github.com)
* [Animate.css](https://daneden.github.io/animate.css)
* [Loaders.css](https://connoratherton.com/loaders)
* [Slick Carousel](https://kenwheeler.github.io/slick)
* [Smooth Scroll](https://github.com/cferdinandi/smooth-scroll)
* [Sticky Kit](http://leafo.net/sticky-kit)
* [JVectorMap](http://jvectormap.com)
* [Font Awesome](https://fontawesome.com)





<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/piper-elinor-townley-4193ab17

<!--[license-shield]
[license-url] -->
