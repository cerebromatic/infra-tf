#! //usr/bin/env bash
set -e

# Environment variables GCP_ORG_ID, GCP_BILLING_ACCOUNT, an ADMIN_PROJECT
# MUST be set in the shell environment prior to executing this script!

export TF_ROOT=${ADMIN_PROJECT}-root
export TF_ROOT_CREDS=~/.config/gcloud/${TF_ROOT}.json
export TF_STATE_BUCKET=${TF_ROOT}-terraform

# Create the Terraform Admin Project
gcloud projects create ${TF_ROOT} \
  --organization ${GCP_ORG_ID} \
  --set-as-default

gcloud beta billing projects link ${TF_ROOT} \
  --billing-account ${GCP_BILLING_ACCOUNT}

# Any actions that Terraform performs require that the API be enabled to do so.
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable cloudbilling.googleapis.com
gcloud services enable iam.googleapis.com
gcloud services enable compute.googleapis.com
gcloud services enable serviceusage.googleapis.com


## Create the Terraform service account

# Create the service account in the Terraform admin project
# and download the JSON credentials:
gcloud iam service-accounts create terraform \
  --display-name "Terraform admin account"

gcloud iam service-accounts keys create ${TF_ROOT_CREDS} \
  --iam-account terraform@${TF_ROOT}.iam.gserviceaccount.com

# Grant the service account permission to view the Admin Project and manage Cloud Storage:
gcloud projects add-iam-policy-binding ${TF_ROOT} \
  --member serviceAccount:terraform@${TF_ROOT}.iam.gserviceaccount.com \
  --role roles/viewer

gcloud projects add-iam-policy-binding ${TF_ROOT} \
  --member serviceAccount:terraform@${TF_ROOT}.iam.gserviceaccount.com \
  --role roles/storage.admin

## Add organization/folder-level permissions

# Grant the service account permission to create projects and assign billing accounts:
gcloud organizations add-iam-policy-binding ${GCP_ORG_ID} \
  --member serviceAccount:terraform@${TF_ROOT}.iam.gserviceaccount.com \
  --role roles/resourcemanager.projectCreator

gcloud organizations add-iam-policy-binding ${GCP_ORG_ID} \
  --member serviceAccount:terraform@${TF_ROOT}.iam.gserviceaccount.com \
  --role roles/billing.user

# Create the remote backend bucket in Cloud Storage
gsutil mb -p ${TF_ROOT} --pap enforced gs://${TF_STATE_BUCKET}

# Enable versioning for the remote bucket:
gsutil versioning set on gs://${TF_STATE_BUCKET}
