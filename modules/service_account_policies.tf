resource "google_project_iam_binding" "this" {
  project = google_project.this.project_id
  role    = "roles/owner"
  members = ["serviceAccount:${google_service_account.this.email}"]
}

resource "google_storage_bucket_iam_binding" "this" {
  bucket  = google_storage_bucket.this.name
  role    = "roles/storage.admin"
  members = ["serviceAccount:${google_service_account.this.email}"]
}
