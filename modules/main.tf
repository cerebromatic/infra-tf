variable "name" {
  type = string
}
variable "org_id" {
  type = string
}
variable "billing_account" {

}
locals {
  prefix = substr(lower(var.name), 0, 3)
}

resource "google_folder" "this" {
  display_name = var.name
  parent       = "organizations/${var.org_id}"
}


resource "random_integer" "this" {
  min = 10000
  max = 100000
}

resource "google_project" "this" {
  name            = "${var.name}-tf"
  project_id      = "${local.prefix}-tf${random_integer.this.result}"
  folder_id       = google_folder.this.id
  billing_account = var.billing_account
}

locals {
  services = [
    "iam.googleapis.com",
    "serviceusage.googleapis.com",
    "compute.googleapis.com",
    "container.googleapis.com"
  ]
}
resource "google_project_service" "this" {
  for_each                   = toset(local.services)
  project                    = google_project.this.project_id
  service                    = each.value
  disable_dependent_services = true
}

resource "google_service_account" "this" {
  account_id   = "${lower(var.name)}-${random_integer.this.result}"
  display_name = "${var.name}-tf"
  project      = google_project.this.project_id
  depends_on = [
    google_project_service.this
  ]
}

resource "google_storage_bucket" "this" {
  name     = "${lower(var.name)}-tf-state"
  project  = google_project.this.project_id
  location = "US"
}

