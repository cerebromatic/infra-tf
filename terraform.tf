terraform {
  backend "gcs" {
    bucket = "cerebromatic-root-terraform"
    prefix = "terraform/state"
  }
}
