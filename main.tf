module "project" {
  source          = "./modules"
  for_each        = toset(local.departments)
  name            = each.key
  org_id          = var.org_id
  billing_account = var.billing_account
}
